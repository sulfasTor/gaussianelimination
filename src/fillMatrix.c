#include "fillMatrix.h"

/* Given by J. J. Bourdin with modifications */
augmtx_t createMatrix (int nb)
{
    augmtx_t m;
    int i, j, k, l, z, zi;

    m.n = nb;
    for (i = 0, k=nb; i < nb; i++, k--) {
	l = k;
	m.m[i][i] = (float) l;	
	l >>= 1;
	for (j = i+1, z=1, zi = 0; j < nb; j++) {
	    if (z == zi) {
		m.m[i][j] = (float) l;
		m.m[j][i] = (float) l;
		l >>= 1;
		z <<= 1;
		zi = 0;
	    }
	    else {
		m.m[i][j] = 0.0;
		m.m[j][i] = 0.0;
		zi++;
	    }
	}
    }
    return m;
}

void fillVector(augmtx_t *mat, int w)
{
    for(int i = 0; i < w; i++)
	mat->m[i][w] = (float) rand() / (float) (RAND_MAX/(w+8)); /* For instance */
}
