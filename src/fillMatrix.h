#ifndef FILLMATRIX_H
#define FILLMATRIX_H
#define MAXMAT 500
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef		float matrix [MAXMAT] [MAXMAT + 1];

typedef struct {
    int n;
    matrix m;
}		augmtx_t; /* Augmented matrix */

typedef struct {
    int n;
    float vec[MAXMAT];
}		vector; /* Solution vector */


augmtx_t	createMatrix (int); /* Creates a square matrix */
void		fillVector(augmtx_t *, int);

#endif /* FILLMATRIX_H */
