#include "gauss.h"
#include "matrixlib.h"

/* int: This function returns the non nul max value in a column 
 * @m: Augmented matrix
 * @n: Size of the matrix
 * @c: Column index
 */

int findPivot(augmtx_t m, int n, int c) {
    int r, pr;
    float pivot;

    pivot = -1.0;
    pr = 0;
    for (r = 0; r < n; r++) {
	if (pivot < fabs(m.m[r][c])) {
	    pivot = fabs(m.m[r][c]);
	    pr = r;
	}
    }
    return (pr);
}

/* void: This function puts the matrix in his reduced row echelon form 
 * @m: Pointer of the augmented matrix
 * @n: Size of the matrix
 */ 
void matrix2ref(augmtx_t *m, int n) {
    int r, j, pr, k, l;
    float pval;
    
    r = 0;
    for (j = 0; j < n && r < n; j++, r++) {	/* Iterate through cols */
	pr = findPivot(*m, n, j);
        //swapRows(m, n+1, r, pr);
        pval = m->m[r][j];
        for (l = j; l < n+1; l++)       /* Normalize row with pivot value */
            m->m[r][l] /= (float) pval;
	for (k = 0; k < n; k++) {               /* Make zero the pivot column */
	    if (k != r) {
		pval = m->m[k][j];
		for (l = j; l < n+1; l++)
		    m->m[k][l] -=  pval * m->m[r][l];
	    }
	}
    }
}

/* vector: This function returns the solution vector
 * @m: Pointer of the augmented matrix
 * @n: Size of the matrix
 */ 
vector	backsubstitution(augmtx_t m, int n) {
    int i;
    vector *sol;

    sol = malloc(sizeof(vector));
    sol->n = n;
    for (i = 0; i < n; i++)
	sol->vec[i] = m.m[i][n];

    return (*sol);
}
