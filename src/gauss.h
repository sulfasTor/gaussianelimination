#ifndef GAUSS_H
#define GAUSS_H
#include "fillMatrix.h"

void matrix2ref(augmtx_t*, int);
vector	backsubstitution(augmtx_t, int);

#endif /* GAUSS_H */
