#include "matrixlib.h"

void	printMatrix(augmtx_t m, int nb) {
    int i, j;

    for (i = 0; i < nb; i++) {
    	for (j = 0; j < nb+1; j++)
	    printf("%.4f ", m.m[i][j]);
	printf("\n");
    }
}

void	printSolution(vector v, int nb, double time) {
    int i;

    printf("The solutions for the matrix of size %dx%d are:\n", nb, nb);
    for(i = 0; i < nb; i++)
	printf("x_%d = %.4f\n", i+1, v.vec[i]);
    printf("It took %f seconds.\n", time);
}

/* This function can be improved */
void	swapRows(augmtx_t *m, int nb, int row_up, int row_down) {
    int i;
    float tmp;

    if (row_up == row_down)
	return;
    
    for (i = 0; i < nb; i++) {
	tmp = m->m[row_up][i];
	m->m[row_up][i] = m->m[row_down][i];
	m->m[row_down][i] = tmp;
    }
}

void verifySolution(vector vec, augmtx_t m, int nb) {
    int i, j;
    float diff;
    float tolerance = 0.05;

    printf("Verification:\n");
    for (i = 0; i < nb; i++) {
	diff = 0;
 	for (j = 0; j < nb; j++)
	    if (m.m[i][j] != 0)
		diff += m.m[i][j] * vec.vec[j];
	printf("Equation n_%d: ", i+1);
	if (fabs(m.m[i][nb] - diff) > tolerance)
	    printf("Difference of %f", (m.m[i][nb] - diff));
	else
	    printf("OK");
	printf("\n");
    }
}
