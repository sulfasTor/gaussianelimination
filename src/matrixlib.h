#ifndef MATRIX_H
#define MATRIX_H
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fillMatrix.h"

void	printMatrix(augmtx_t, int);
void	printSolution(vector, int, double);
void	swapRows(augmtx_t *, int, int, int);
void	verifySolution(vector, augmtx_t, int);
#endif
