#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "fillMatrix.h"
#include "matrixlib.h"
#include "gauss.h"

int main(int argc, char **argv)
{
    augmtx_t m;
    augmtx_t *m_old;
    int nb, i, j;
    vector res;
    clock_t start, end;
    double cpu_time_used;

    if (argc == 1) {
	printf("usage: ./solve_gauss <square matrix dimension>\n");
	exit(0);
    }
    
    nb = atoi(argv[1]);
    if (nb > MAXMAT) {
	printf("Matrix is too big!\n");
	exit(0);
    } else if (nb <= 0) {
	printf("Nothing to do here. Tchau.\n");
	exit(0);
    }
	
    m = createMatrix(nb);
    fillVector(&m, nb);
    printMatrix(m, nb);
    printf("\n");

    /* Copy matrix */
    m_old = malloc(sizeof(augmtx_t));
    for (i=0; i < nb; i++)
	for (j=0; j < nb+1; j++)
	    m_old->m[i][j] = m.m[i][j];
    
    /* Gauss-Jordan Method RREF */
    start = clock();
    matrix2ref(&m, nb);
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    
    /* Backsubstitution */
    res = backsubstitution(m, nb);

    printSolution(res, nb, cpu_time_used); /* Prints the solution vector */
    printf("\n");
    verifySolution(res, *m_old, nb);

    return 0;
}


