#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "fillMatrix.h"
#include "matrixlib.h"
#include "gauss.h"

void gauss_operation_no_result(int nb);

int main(int argc, char **argv)
{
    int i, nb, nb_ops;
    vector res;
    clock_t start, end;
    double cpu_time_used;

    if (argc == 1) {
	printf("usage: ./solve_gauss <square matrix dimension>\n");
	exit(0);
    }
    
    nb = atoi(argv[1]);
    if (nb > MAXMAT) {
	printf("Matrix is too big!\n");
	exit(0);
    }
    
    nb_ops = 1;
    start = clock();
    for (i = 0; i < nb_ops; i++)
	gauss_operation_no_result(nb);
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("%f\n", 1000 * cpu_time_used / (double) nb_ops);

    return 0;
}

void gauss_operation_no_result(int nb) {
    augmtx_t m;

    m = createMatrix(nb);
    fillVector(&m, nb);
    matrix2ref(&m, nb);
}

