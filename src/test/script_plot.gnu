# gnuplot --persist script_plot.gnu

set term png
set title "Performance du pivot de Gauss"
set grid
set xlabel "Taille n"
set ylabel "Temps écoulé moyen (ms)"
set datafile separator ","
set yrange [0:250]
# set xrange [0:100]
# set ytics 0.5

# Linear regression
f(x) = a + b*x + c*x**2 + d*x**3
fit f(x) 'resultats.csv' via a,b,c,d
title_f(a,b,c,d) = sprintf('f(x) = %.2fx^3 + %.2fx^2 + %.2fx + %2.f', d,c,b,a)

set output 'plot.png'
plot 'resultats.csv' w l notitle, f(x) t title_f(a,b,c,d)